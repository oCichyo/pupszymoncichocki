<?php

require('./fpdf/fpdf.php');
require('./tfpdf/tfpdf.php');


$pdf=new tFPDF();
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu','B','DejaVuSansCondensed-Bold.ttf',true);
$pdf->SetFont('DejaVu','',14);

$pdf->AddPage();

$pdf->Image('logo.png',10,15,-300);

//$pdf->SetFont('ubuntu','',16); 

$pdf->Ln();
$pdf->SetFont('DejaVu','B',16);
$pdf->SetXY(77,5); 
$pdf->Cell(5,6, 'ZLECENIE SPEDYCJI ');
$pdf->Ln();

$pdf->SetXY(125,25); 
$pdf->Cell(5,6, 'NR: ' . $_POST['nr_zlecenia']);
$pdf->Ln();




$pdf->SetFont('DejaVu','B',14);
$pdf->SetXY(7,35); 
$pdf->Cell(10,20, 'Zleceniodawca: ');
$pdf->Ln();

$pdf->SetFont('DejaVu','',12);
$pdf->SetXY(7,44); 
$pdf->Cell(10,15,  $_POST['zleceniodawca']);
$pdf->Ln();

$pdf->SetFont('DejaVu','B',11);
$pdf->SetXY(7,47); 
$pdf->Cell(5,24, 'NIP:' . $_POST['nip_z']);
$pdf->Ln();

$pdf->SetFont('DejaVu','B',14);
$pdf->SetXY(108,35); 
$pdf->Cell(10,20, 'Zleceniobiorca:');
$pdf->Ln();

$pdf->SetFont('DejaVu','',12);
$pdf->SetXY(108,44); 
$pdf->Cell(10,15,  $_POST['zleceniobiorca']);
$pdf->Ln();

$pdf->SetFont('DejaVu','B',11);
$pdf->SetXY(108,47); 
$pdf->Cell(5,24, 'NIP:' . $_POST['nip_o']);
$pdf->Ln();

$pdf->SetFont('DejaVu','B',14);
$pdf->SetXY(7,59); 
$pdf->Cell(10,20, 'Kierowca:');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',14);
$pdf->SetXY(108,59); 
$pdf->Cell(10,20, 'Pojazd:');
$pdf->Ln();

$pdf->SetFont('DejaVu','',12);
$pdf->SetXY(7,72); 
$pdf->Cell(10,15, $_POST['d_kierowcy']);
$pdf->Ln();

$pdf->SetFont('DejaVu','',12);
$pdf->SetXY(108,72); 
$pdf->Cell(10,15, $_POST['d_pojazdu']);
$pdf->Ln();

$pdf->SetFont('DejaVu','B',14);
$pdf->SetXY(7,84); 
$pdf->Cell(10,20, 'Załadunek:');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',12);
$pdf->SetXY(7,94); 
$pdf->Cell(10,15, 'Data i godzina');
$pdf->Ln();

$pdf->SetFont('DejaVu','',12);
$pdf->SetXY(7,100); 
$pdf->Cell(10,15, $_POST['data_zaladunku']);
$pdf->Ln();

$pdf->SetFont('DejaVu','B',12);
$pdf->SetXY(55,94); 
$pdf->Cell(10,15, 'Adres');
$pdf->Ln();

$pdf->SetFont('DejaVu','',12);
$pdf->SetXY(55,100); 
$pdf->Cell(10,15, $_POST['zal_adres']);
$pdf->Ln();

$pdf->SetFont('DejaVu','B',12);
$pdf->SetXY(145,94); 
$pdf->Cell(10,15, 'Nazwa firmy');
$pdf->Ln();

$pdf->SetFont('DejaVu','',12);
$pdf->SetXY(145,100); 
$pdf->Cell(10,15,$_POST['zal_frima'] );
$pdf->Ln();

$pdf->SetFont('DejaVu','B',14);
$pdf->SetXY(7,109); 
$pdf->Cell(10,20, 'Rozładunek:');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',12);
$pdf->SetXY(7,119); 
$pdf->Cell(10,15, 'Data i godzina');
$pdf->Ln();

$pdf->SetFont('DejaVu','',12);
$pdf->SetXY(7,125); 
$pdf->Cell(10,15, $_POST['data_rozladunku']);
$pdf->Ln();

$pdf->SetFont('DejaVu','B',12);
$pdf->SetXY(55,119); 
$pdf->Cell(10,15, 'Adres');
$pdf->Ln();

$pdf->SetFont('DejaVu','',12);
$pdf->SetXY(55,125); 
$pdf->Cell(10,15, $_POST['rozl_adres']);
$pdf->Ln();

$pdf->SetFont('DejaVu','B',12);
$pdf->SetXY(145,119); 
$pdf->Cell(10,15, 'Nazwa firmy');
$pdf->Ln();

$pdf->SetFont('DejaVu','',12);
$pdf->SetXY(145,125); 
$pdf->Cell(10,15,$_POST['rozl_firma'] );
$pdf->Ln();


$pdf->SetFont('DejaVu','B',14);
$pdf->SetXY(7,136); 
$pdf->Cell(10,15, 'Ładunek:');
$pdf->Ln();

$pdf->SetFont('DejaVu','',12);
$pdf->SetXY(7,142); 
$pdf->Cell(10,15,$_POST['towar'] );
$pdf->Ln();



$pdf->SetFont('DejaVu','B',14);
$pdf->SetXY(73,136); 
$pdf->Cell(10,15, 'Fracht:');
$pdf->Ln();

$pdf->SetFont('DejaVu','',12);
$pdf->SetXY(73,142); 
$pdf->Cell(10,15,$_POST['fracht'] );
$pdf->Ln();

$pdf->SetFont('DejaVu','B',14);
$pdf->SetXY(139,136); 
$pdf->Cell(10,15, 'Termin płatności:');
$pdf->Ln();

$pdf->SetFont('DejaVu','',12);
$pdf->SetXY(139,142); 
$pdf->Cell(10,15, $_POST['termin_platnosci']);
$pdf->Ln();

$pdf->SetFont('DejaVu','B',14);
$pdf->SetXY(7,151); 
$pdf->Cell(10,15, 'Uwagi:');
$pdf->Ln();

$pdf->SetFont('DejaVu','',12);
$pdf->SetXY(7,159); 
$pdf->Cell(10,15, $_POST['uwagi']);
$pdf->Ln();

$pdf->SetFont('DejaVu','B',14);
$pdf->SetXY(7,172); 
$pdf->Cell(10,15, 'Podpis i pieczęć zleceniodawcy:');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',14);
$pdf->SetXY(107,172); 
$pdf->Cell(10,15, 'Podpis i pieczęć zleceniobiorcy:');
$pdf->Ln();


$pdf->SetFont('DejaVu','B',9);
$pdf->SetXY(77,200); 
$pdf->Cell(10,15, 'Ogólne warunki zlecenia spedycyjnego:');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',9);
$pdf->SetXY(5,206); 
$pdf->Cell(10,15, '1. Zleceniodawca zleca a Zleceniobiorca przyjmuje do wykonania usługę spedycyjną, na zasadach określonych w niniejszych ');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',9);
$pdf->SetXY(8,210); 
$pdf->Cell(10,15, ' Ogólnych Warunkach Zlecenia Spedycyjnego (dalej:OWZS) oraz Zleceniu Spedycyjnym (dalej: ZS).');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',9);
$pdf->SetXY(5,214); 
$pdf->Cell(10,15, '2. Do wykonania zlecenia mają zastosowanie przepisy Konwencji o umowie międzynarodowego przewozu drogowego');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',9);
$pdf->SetXY(8,218); 
$pdf->Cell(10,15, ' towarów (CMR) i Protokół podpisania, sporządzone w Genewie 19 maja 1956 r z dnia 19 maja 1956 r.');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',9);
$pdf->SetXY(8,222); 
$pdf->Cell(10,15, '(Dz.U. 1962 Nr 49, poz. 238) Prawa przewozowego i Kodeksu cywilnego, o ile OWZS i ZS nie regulują zlecenia odmiennie.');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',9);
$pdf->SetXY(5,226); 
$pdf->Cell(10,15, '3. Zleceniodawca odpowiada za wszelkie koszty i szkody Zleceniobiorcy poniesione na skutek działania lub zaniechania');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',9);
$pdf->SetXY(7,230); 
$pdf->Cell(10,15, ' własnego, osób za których odpowiedzialność ponosi lub które działają w jego imieniu lub na jego rzecz.');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',9);
$pdf->SetXY(7,234); 
$pdf->Cell(10,15, ' Odpowiedzialność za niewykonanie lub nienależyte wykonanie ZS przez Zleceniobiorcę ogranicza się do wysokości ');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',9);
$pdf->SetXY(7,238); 
$pdf->Cell(10,15, ' wynagrodzenia za fracht.');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',9);
$pdf->SetXY(5,242); 
$pdf->Cell(10,15, '4. Jeżeli Zleceniodawca powierza Zleceniobiorcy towary niebezpieczne, powinien mu dokładnie opisać, jakie ');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',9);
$pdf->SetXY(9,246); 
$pdf->Cell(10,15, 'niebezpieczeństwo przedstawiają, i wskazać mu w razie potrzeby, jakie środki ostrożności należy podjąć.');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',9);
$pdf->SetXY(5,250); 
$pdf->Cell(10,15, '5. Zleceniodawca oświadcza, iż wartość towaru w miejscu i w okresie przyjęcia go do przewozu równa jest wartości ');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',9);
$pdf->SetXY(8,254); 
$pdf->Cell(10,15, ' wynagrodzenia Zleceniobiorcy za fracht. W odmiennym przypadku Zleceniodawca zobowiązany jest zgłosić to ');
$pdf->Ln();

$pdf->SetFont('DejaVu','B',9);
$pdf->SetXY(9,258); 
$pdf->Cell(10,15, 'Zleceniobiorcy i zadeklarować wartość wyższą, co stanowi podstawę do podwyższenia wynagrodzenia Zleceniobiorcy.');
$pdf->Ln();




















$pdf->SetXY(5,2); //tytulowa kolumna 1x
$pdf->Cell(200,12,"",1,0,'C',0);
$pdf->Ln();

$pdf->SetXY(5,14); //tytulowa kolumna 2x
$pdf->Cell(200,25,"",1,0,'C',0);
$pdf->Ln();

$pdf->SetXY(5,39); //tytulowa kolumna 3x1
$pdf->Cell(200,25,"",1,0,'C',0);
$pdf->Ln();

$pdf->SetXY(5,39); //tytulowa kolumna 3x2
$pdf->Cell(100,25,"",1,0,'C',0);
$pdf->Ln();

$pdf->SetXY(5,64); //tytulowa kolumna 4x
$pdf->Cell(200,25,"",1,0,'C',0);
$pdf->Ln();

$pdf->SetXY(5,89); //tytulowa kolumna 5x
$pdf->Cell(200,25,"",1,0,'C',0);
$pdf->Ln();

$pdf->SetXY(5,114); //tytulowa kolumna 6x
$pdf->Cell(200,25,"",1,0,'C',0);
$pdf->Ln();

$pdf->SetXY(5,139); //tytulowa kolumna 7x1
$pdf->Cell(200,15,"",1,0,'C',0);
$pdf->Ln();

$pdf->SetXY(5,139); //tytulowa kolumna 7x2
$pdf->Cell(66,15,"",1,0,'C',0);
$pdf->Ln();

$pdf->SetXY(71,139); //tytulowa kolumna 7x2
$pdf->Cell(66,15,"",1,0,'C',0);
$pdf->Ln();

$pdf->SetXY(5,154); //tytulowa kolumna 8X
$pdf->Cell(200,20,"",1,0,'C',0);
$pdf->Ln();

$pdf->SetXY(5,174); //tytulowa kolumna 9X1
$pdf->Cell(100,30,"",1,0,'C',0);
$pdf->Ln();

$pdf->SetXY(105,174); //tytulowa kolumna 9X1
$pdf->Cell(100,30,"",1,0,'C',0);
$pdf->Ln();












$pdf->Output();

//print_r($_POST);

//echo $_POST['zleceniodawca'];

