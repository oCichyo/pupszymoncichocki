<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Zlecenie spedycyjne</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <style>

            body {
                height: 842px;
                width: 595px;

                margin-left: auto;
                margin-right: auto;

            }

            .myButton {
                -moz-box-shadow: 0px 10px 14px -7px #3e7327;
                -webkit-box-shadow: 0px 10px 14px -7px #3e7327;
                box-shadow: 0px 10px 14px -7px #3e7327;
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #77b55a), color-stop(1, #72b352));
                background:-moz-linear-gradient(top, #77b55a 5%, #72b352 100%);
                background:-webkit-linear-gradient(top, #77b55a 5%, #72b352 100%);
                background:-o-linear-gradient(top, #77b55a 5%, #72b352 100%);
                background:-ms-linear-gradient(top, #77b55a 5%, #72b352 100%);
                background:linear-gradient(to bottom, #77b55a 5%, #72b352 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#77b55a', endColorstr='#72b352',GradientType=0);
                background-color:#77b55a;
                -moz-border-radius:4px;
                -webkit-border-radius:4px;
                border-radius:4px;
                border:1px solid #4b8f29;
                display:inline-block;
                cursor:pointer;
                color:#ffffff;
                font-family:Arial;
                font-size:13px;
                font-weight:bold;
                padding:6px 12px;
                text-decoration:none;
                text-shadow:0px 1px 0px #5b8a3c;
                margin-left: 45%;
                margin-top: 2%;
                margin-bottom: 2%;
            }
            .myButton:hover {
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #72b352), color-stop(1, #77b55a));
                background:-moz-linear-gradient(top, #72b352 5%, #77b55a 100%);
                background:-webkit-linear-gradient(top, #72b352 5%, #77b55a 100%);
                background:-o-linear-gradient(top, #72b352 5%, #77b55a 100%);
                background:-ms-linear-gradient(top, #72b352 5%, #77b55a 100%);
                background:linear-gradient(to bottom, #72b352 5%, #77b55a 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#72b352', endColorstr='#77b55a',GradientType=0);
                background-color:#72b352;
            }
            .page-header{
                -moz-box-shadow: 0px 10px 14px -7px #3e7327;
                -webkit-box-shadow: 0px 10px 14px -7px #3e7327;
                box-shadow: 0px 10px 14px -7px #3e7327;
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #77b55a), color-stop(1, #72b352));
                background:-moz-linear-gradient(top, #77b55a 5%, #72b352 100%);
                background:-webkit-linear-gradient(top, #77b55a 5%, #72b352 100%);
                background:-o-linear-gradient(top, #77b55a 5%, #72b352 100%);
                background:-ms-linear-gradient(top, #77b55a 5%, #72b352 100%);
                background:linear-gradient(to bottom, #77b55a 5%, #72b352 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#77b55a', endColorstr='#72b352',GradientType=0);
                background-color:#77b55a;
                -moz-border-radius:4px;
                -webkit-border-radius:4px;
                border-radius:4px;
                border:1px solid #4b8f29;
                display:inline-block;
                cursor:pointer;
                color:#ffffff;
                font-family:Arial;
                font-size:13px;
                font-weight:bold;
                padding:6px 12px;
                text-decoration:none;
                text-shadow:0px 1px 0px #5b8a3c;
                }
                .text_header {
                    margin-left: 8%;
                    
                    
                }
                .text_pod
                {
                    margin-left: 4%;
                    
                }
        </style>



    </head>
    <body>
        <div class = "page-header">
            <h1 class="text_header">
                Witamy w kreatorze  zlecenia spedycyjnego!
            </h1>
            
        </div>

        <p class ="text_pod">Aby otrzymać zlecenie spedycyjne w formie pliku PDF, uzupełnij wszystkie pola i naciśnij przycisk "Druk PDF"</p>




        <form action="pdf.php" method="POST">

            <div class="input-group" style = "padding: 20px 10px 10px;">
                <span class="input-group-addon" id="basic-addon1">1. Numer zlecenia:</span>
                <input type="text" class="form-control" placeholder="np. 31/S/2016" aria-describedby="basic-addon1" name="nr_zlecenia">
                       
            </div>

            <div class="input-group" style = "padding: 20px 10px 10px;">
                <span class="input-group-addon" id="basic-addon1">2. Zleceniodawca:</span>
                <input type="text" class="form-control" placeholder="nazwa firmy, adres" aria-describedby="basic-addon1" name="zleceniodawca">
                <input type="text" class="form-control" placeholder="NIP" aria-describedby="basic-addon1" name="nip_z">
            </div>

            <div class="input-group" style = "padding: 20px 10px 10px;">
                <span class="input-group-addon" id="basic-addon1">3. Zleceniobiorca:</span>
                <input type="text" class="form-control" placeholder="nazwa firmy, adres" aria-describedby="basic-addon1" name="zleceniobiorca">
                <input type="text" class="form-control" placeholder="NIP" aria-describedby="basic-addon1" name="nip_o">
            </div>

            <div class="input-group" style = "padding: 20px 10px 10px;">
                <span class="input-group-addon" id="basic-addon1">4. Miejsce załadunku:</span>
                <input type="text" class="form-control" placeholder="nazwa przedsiębiorstwa" aria-describedby="basic-addon1" name="zal_frima">
                 <input type="text" class="form-control" placeholder="ulica, kod pocztowy, miasto" aria-describedby="basic-addon1" name="zal_adres">
            </div>

            <div class="input-group" style = "padding: 20px 10px 10px;">
                <span class="input-group-addon" id="basic-addon1">5. Miejsce rozładunku:</span>
                <input type="text" class="form-control" placeholder="nazwa przedsiębiorstwa" aria-describedby="basic-addon1" name="rozl_firma">
                <input type="text" class="form-control" placeholder="ulica, kod pocztowy, miasto" aria-describedby="basic-addon1" name="rozl_adres">
            </div>

            <div class="input-group" style = "padding: 20px 10px 10px;">
                <span class="input-group-addon" id="basic-addon1">6. Data załadunku:</span>
                <input type="text" class="form-control" placeholder="data załadunku i godzina" aria-describedby="basic-addon1" name="data_zaladunku">
            </div>

            <div class="input-group" style = "padding: 20px 10px 10px;">
                <span class="input-group-addon" id="basic-addon1">7. Data rozładunku:</span>
                <input type="text" class="form-control" placeholder="data rozładunku  i godzina" aria-describedby="basic-addon1" name="data_rozladunku">
            </div>
            
            <div class="input-group" style = "padding: 20px 10px 10px;">
                <span class="input-group-addon" id="basic-addon1"> 8. Towar:</span>
                <input type="text" class="form-control" placeholder="np. 33 palet euro" aria-describedby="basic-addon1" name="towar">
            </div>
            
            <div class="input-group" style = "padding: 20px 10px 10px;">
                <span class="input-group-addon" id="basic-addon1"> 9. Fracht:</span>
                <input type="text" class="form-control" placeholder=" np. 1000 zl" aria-describedby="basic-addon1" name="fracht">
            </div>
            
            <div class="input-group" style = "padding: 20px 10px 10px;">
                <span class="input-group-addon" id="basic-addon1"> 10. Pojazd i kierowca:</span>
                <input type="text" class="form-control" placeholder="dane kierowcy, telefon" aria-describedby="basic-addon1" name="d_kierowcy">
                <input type="text" class="form-control" placeholder="tablice pojazdu" aria-describedby="basic-addon1" name="d_pojazdu">
            </div>

            
            <div class="input-group" style = "padding: 20px 10px 10px;">
                <span class="input-group-addon" id="basic-addon1">11. Termin płatności:</span>
                <input type="text" class="form-control" placeholder="np. 45 dni" aria-describedby="basic-addon1" name="termin_platnosci">
            </div>
            
            <div class="input-group" style = "padding: 20px 10px 10px;">
                <span class="input-group-addon" id="basic-addon1">12. Uwagi:</span>
                <input type="text" class="form-control" placeholder=" np załadunek bokiem" aria-describedby="basic-addon1" name="uwagi">
            </div>

            



            <INPUT type="submit" value="Druk PDF" class="myButton">

            </table>
            </TABLE>
        </form>











    </body>
</html>








